$(document).bind('keydown', function (evt){
	restrictKey(evt);

});

// Browser Back button Control
//window.onunload = function(){};
//history.forward();

function restrictKey(ev) {
	//debugger;
    var keycode, shift, ctrl, alt;
    // text, password, textarea,..
    var el = ev.target;

    // キーコード及びctrl、shift、alt有無を取得
    if (ev.which) {
        keycode = ev.which;
        ctrl  = typeof ev.modifiers == 'undefined' ? ev.ctrlKey  : ev.modifiers & Event.CONTROL_MASK;
        shift = typeof ev.modifiers == 'undefined' ? ev.shiftKey : ev.modifiers & Event.SHIFT_MASK;
        alt   = typeof ev.modifiers == 'undefined' ? ev.altKey   : ev.modifiers & Event.ALT_MASK;
    } else {
        keycode = ev.keyCode;
        ctrl    = ev.ctrlKey;
        shift   = ev.shiftKey;
        alt     = ev.altKey;
    }

    //console.log("keycode:"+keycode+",ctrl:"+ctrl+",shift:"+shift+",alt:"+alt);
    // ctrlキー押下時の禁止動作
    if (ctrl) {
        switch (keycode){
	         case 66: // Ctrl + B（お気に入りダイアログを表示する。）
	         case 68: // Ctrl + D（お気に入りに追加する。）
	         case 69: // Ctrl + E（検索バーを表示する。）
	         case 72: // Ctrl + H（履歴バーを表示する。）
	         case 73: // Ctrl + I（お気に入りバーを表示する。）
	         case 76: // Ctrl + L（新しいＵＲＬ・ファイルを開く。）
	         case 78: // Ctrl + N（新しいウィンドウを表示する。）
	         case 79: // Ctrl + O （新しいＵＲＬ・ファイルを開く。）
	         case 80: // Ctrl + P（ページを印刷する。）
	         case 82: // Ctrl + R（画面を更新する。）
	         case 83: // Ctrl + S（ページを保存する。）
	         case 85: // Ctrl + U（ソースをビュー。）
	         case 87: // Ctrl + W（ウィンドウを閉じる。）
	         case 116:// Ctrl + F5 （画面を更新する）
	               eventStop(ev);
	               break;
          default:
        }
    // altキー押下時の禁止動作
    } else if (alt) {
        switch (keycode) {
             case 39: // Alt + →（次のページに移動する。）
             case 37: // Alt + ← （前のページに移動する。）

                //ev.preventDefault();
            	eventStop(ev);
                break;
          default:
        }
    // shiftキー押下時の禁止動作
    } else if (shift) {
        switch (keycode) {
          case 16: //ShiftキーのみはOK
        	  break;
          case 121: // Shift + F10（コンテキストメニュー表示を表示する）
              //ev.preventDefault();
        	  eventStop(ev);
              break;
         default:
        }
    //その他キー押下時の禁止動作
    } else {
        switch (keycode) {
        	 case 13: // Enter
        		 if (el.type != "textarea") {
        			 console.log(el.nodeName+" " + el.type+" " + el.name);
        			 // Allow Enter on button, link, keyword (search) and  login submit
        			 if (el.nodeName == "A" ||
        					 (el.nodeName == "INPUT" && (el.type=="submit" || el.type =="button" || el.name=="keyword"))) {
        				 // do nothing
        			 } else {
        				 var frm = document.getElementsByTagName("form")[0];
        				 // Login form: allow enter to submit login
        				 // and some forms too
        				 if (frm.name != "login" && frm.name != "VU007" && frm.name != "VU017" &&
        					 frm.name != "OU007" && frm.name != "OU017") {
        					 eventStop(ev);
        				 }
        			 }
        		 }
        		 break;

             case 27: // Esc（ページの読み込みを中止する）
	         case 112: // F1 Help
	         case 113: // F2
	         case 114: // F3 （検索バーを表示する。）
	         case 115: // F4 （アドレスバーリスト）
	         case 116: // F5  （画面を更新する）
	         case 117: // F6 （アドレスバーをフォカース。）
	         case 118: // F7 （caret browsing mode）
	         case 122: // F11 （全画面表示と通常画面表示の切り替えを行う。）
	         case 123: // F12 Inspectを開く
                   eventStop(ev);
                   break;
             default:
          }
       }
}

// イベントキャンセル
function eventStop(ev) {
	ev.stopPropagation();
	ev.preventDefault();
    try {ev.keyCode = 0; } catch (e) {}
}
