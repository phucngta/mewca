/*
 * Main javascript
 */

jQuery(function($) {


});

/**
 * Remove element in array
 * @param arr
 * @param element
 * @returns
 */
function js_main_removeElementInArray(arr, element) {
	return arr.splice($.inArray(element, arr), 1);
}

/**
 * Explain about function name: js_main_getListChecked.
 * 		js is folder
 * 		main is main.js
 * 		getListChecked: function name
 * Get list name when checked id
 * Current use for VA001.jsp
 */
function js_main_getListChecked() {
	var listName = [];
	var listId = [];
	$('.listRecId').click(function() {
		id = this.value;
		name = $('#recid-'+id).html();
		if(this.checked == true) {
			listName.push(name);
			listId.push(id);
		} else {
			js_main_removeElementInArray(listName, name);
			js_main_removeElementInArray(listId, id);
		}
		$('#listNameChecked').val(listName.toString().replace(/,/g, ', '));
		$('#listIdChecked').val(listId.toString());
	});
}

/**
 * Process use when press keyboard enter
 * @param event
 * @returns {Boolean}
 */
function pressEnter(event) {
	var code = event.keyCode || event.which;
 	if(code == 13) { //Enter keycode
   		return true;
 	}
 	return false;
}

/**
 * Auto copy text from textbox to textbox
 *
 */
function autoFillText(textFrom, textTo){
	var valTextFrom = $("input[name="+textFrom+"]").val();
	if(valTextFrom != "" && isAlphabet(valTextFrom)){
		$("input[name="+textTo+"]").val(valTextFrom);
	}
}
/**
 * Check text enter to textbox: english or not
 * @param text
 * @returns {Boolean}
 */
function isAlphabet(text){
	english = /^[A-Za-z ]*$/;
	if(english.test(text) == false){
		return false;
	}
	return true;
}

function disableButton(btn, formId, allDisable) {
	btn.disabled = true;

	var e = document.createElement('input');
	e.setAttribute('type', 'hidden');
	e.setAttribute('name', btn.name);
	e.setAttribute('value', btn.value);

	var form = document.getElementById(formId);
	form.appendChild(e);

	if (allDisable) {
		var elements = form.elements;
		for (var i = 0; i < elements.length; i++) {
			if (elements[i].type == 'submit') {
				elements[i].disabled = true;
			}
		}
	}

	var userAgent = window.navigator.userAgent.toLowerCase();
	if (userAgent.indexOf('chrome') != -1) {
		form.submit();
	}
}