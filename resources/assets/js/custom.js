///// Show/hidden content post Facebook
$('#more').on('click', function(){
    $(this).removeClass('visible').addClass('hidden');
    $('#hidden-content').removeClass('hidden').addClass('visible');
});

$('#less').on('click', function(){
    $('#more').removeClass('hidden').addClass('visible');
    $('#hidden-content').removeClass('visible').addClass('hidden');
});


///// Show comment Facebook
$('#comment').on('click', function(){
    $('.postComment').removeClass('hidden').addClass('visible');
});

///// Modal share post
// Get the modal share
var modal = document.getElementById('modal-share');

// Get the button that opens the modal
var btn_share = document.getElementById("share");

// Get the <span> element that closes the modal
var close_share = document.getElementsByClassName("close-share")[0];

// When the user clicks on the button, open the modal
btn_share.onclick = function() {
    modal.style.display = "block";
}

// When the user clicks on <close_share> (x), close the modal
close_share.onclick = function() {
    modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}

///// Modal image
// Get the modal
var modal_img = document.getElementById('modal-image');

// Get the image and insert it inside the modal - use its "alt" text as a caption
var img = document.getElementById('img01');
var img_content = document.getElementById("img-content");
var captionText = document.getElementById("caption");
img.onclick = function(){
    modal_img.style.display = "block";
    img_content.src = this.src;
    captionText.innerHTML = this.alt;
}

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
    modal_img.style.display = "none";
}



