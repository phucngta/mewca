|-- undefined
    |-- .bowerrc
    |-- .gitignore
    |-- bower.json
    |-- Gruntfile.js
    |-- package.json
    |-- README.md
    |-- setup.json
    |-- .idea
    |   |-- jsLibraryMappings.xml
    |   |-- mewca.iml
    |   |-- modules.xml
    |   |-- vcs.xml
    |   |-- workspace.xml
    |-- helpers
    |   |-- config.js
    |   |-- task-configs
    |   |   |-- .htmlhintrc
    |   |   |-- .jshintrc
    |   |   |-- csscomb.json
    |   |   |-- jscs.airbnb.json
    |   |   |-- stylelint.config.js
    |   |-- templates
    |   |   |-- svg-sprites
    |   |       |-- stylesheet.hbs
    |   |-- _grunt
    |       |-- browserify.js
    |       |-- browserSync.js
    |       |-- clean.js
    |       |-- combine_mq.js
    |       |-- concurrent.js
    |       |-- copy.js
    |       |-- csscomb.js
    |       |-- cssmin.js
    |       |-- dr-svg-sprites.js
    |       |-- express.js
    |       |-- htmlhint.js
    |       |-- jscs.js
    |       |-- jshint.js
    |       |-- mangony.js
    |       |-- open.js
    |       |-- phantomas.js
    |       |-- photobox.js
    |       |-- postcss.js
    |       |-- postcssSeparator.js
    |       |-- replace.js
    |       |-- responsive_images.js
    |       |-- sass.js
    |       |-- sassGlobber.js
    |       |-- stylelint.js
    |       |-- sync.js
    |       |-- uglify.js
    |       |-- version.js
    |       |-- watch.js
    |       |-- webdriver.js
    |-- resources
    |   |-- .gitignore
    |   |-- assets
    |   |   |-- css
    |   |   |   |-- custom.css
    |   |   |   |-- font-awesome.min.css
    |   |   |   |-- jquery-ui-1.9.2.min.css
    |   |   |   |-- jquery.bxslider.css
    |   |   |   |-- main.css
    |   |   |   |-- reaction.css
    |   |   |-- fonts
    |   |   |   |-- fontawesome-webfont.eot
    |   |   |   |-- fontawesome-webfont.svg
    |   |   |   |-- fontawesome-webfont.ttf
    |   |   |   |-- fontawesome-webfont.woff
    |   |   |   |-- fontawesome-webfont.woff2
    |   |   |   |-- FontAwesome.otf
    |   |   |-- img
    |   |   |   |-- cake-images.jpg
    |   |   |   |-- default_avatar.gif
    |   |   |   |-- facebook.png
    |   |   |   |-- favicon.jpg
    |   |   |   |-- logo.png
    |   |   |   |-- reaction-small.png
    |   |   |   |-- reactionsgif.gif
    |   |   |   |-- reactions_angry.png
    |   |   |   |-- reactions_haha.png
    |   |   |   |-- reactions_like.png
    |   |   |   |-- reactions_love.png
    |   |   |   |-- reactions_sad.png
    |   |   |   |-- reactions_wow.png
    |   |   |   |-- tagline.png
    |   |   |-- js
    |   |       |-- chart.js
    |   |       |-- common.js
    |   |       |-- custom.js
    |   |       |-- jquery-ui-1.9.2.min.js
    |   |       |-- jquery.bxslider.min.js
    |   |       |-- jquery.min.js
    |   |       |-- main.js
    |   |       |-- reaction.js
    |   |-- bower-components
    |   |   |-- bootstrap-sass
    |   |   |   |-- .bower.json
    |   |   |   |-- bower.json
    |   |   |   |-- CHANGELOG.md
    |   |   |   |-- composer.json
    |   |   |   |-- CONTRIBUTING.md
    |   |   |   |-- eyeglass-exports.js
    |   |   |   |-- LICENSE
    |   |   |   |-- package.json
    |   |   |   |-- README.md
    |   |   |   |-- sache.json
    |   |   |   |-- assets
    |   |   |       |-- fonts
    |   |   |       |   |-- bootstrap
    |   |   |       |       |-- glyphicons-halflings-regular.eot
    |   |   |       |       |-- glyphicons-halflings-regular.svg
    |   |   |       |       |-- glyphicons-halflings-regular.ttf
    |   |   |       |       |-- glyphicons-halflings-regular.woff
    |   |   |       |       |-- glyphicons-halflings-regular.woff2
    |   |   |       |-- images
    |   |   |       |-- javascripts
    |   |   |       |   |-- bootstrap-sprockets.js
    |   |   |       |   |-- bootstrap.js
    |   |   |       |   |-- bootstrap.min.js
    |   |   |       |   |-- bootstrap
    |   |   |       |       |-- affix.js
    |   |   |       |       |-- alert.js
    |   |   |       |       |-- button.js
    |   |   |       |       |-- carousel.js
    |   |   |       |       |-- collapse.js
    |   |   |       |       |-- dropdown.js
    |   |   |       |       |-- modal.js
    |   |   |       |       |-- popover.js
    |   |   |       |       |-- scrollspy.js
    |   |   |       |       |-- tab.js
    |   |   |       |       |-- tooltip.js
    |   |   |       |       |-- transition.js
    |   |   |       |-- stylesheets
    |   |   |           |-- _bootstrap-compass.scss
    |   |   |           |-- _bootstrap-mincer.scss
    |   |   |           |-- _bootstrap-sprockets.scss
    |   |   |           |-- _bootstrap.scss
    |   |   |           |-- bootstrap
    |   |   |               |-- _alerts.scss
    |   |   |               |-- _badges.scss
    |   |   |               |-- _breadcrumbs.scss
    |   |   |               |-- _button-groups.scss
    |   |   |               |-- _buttons.scss
    |   |   |               |-- _carousel.scss
    |   |   |               |-- _close.scss
    |   |   |               |-- _code.scss
    |   |   |               |-- _component-animations.scss
    |   |   |               |-- _dropdowns.scss
    |   |   |               |-- _forms.scss
    |   |   |               |-- _glyphicons.scss
    |   |   |               |-- _grid.scss
    |   |   |               |-- _input-groups.scss
    |   |   |               |-- _jumbotron.scss
    |   |   |               |-- _labels.scss
    |   |   |               |-- _list-group.scss
    |   |   |               |-- _media.scss
    |   |   |               |-- _mixins.scss
    |   |   |               |-- _modals.scss
    |   |   |               |-- _navbar.scss
    |   |   |               |-- _navs.scss
    |   |   |               |-- _normalize.scss
    |   |   |               |-- _pager.scss
    |   |   |               |-- _pagination.scss
    |   |   |               |-- _panels.scss
    |   |   |               |-- _popovers.scss
    |   |   |               |-- _print.scss
    |   |   |               |-- _progress-bars.scss
    |   |   |               |-- _responsive-embed.scss
    |   |   |               |-- _responsive-utilities.scss
    |   |   |               |-- _scaffolding.scss
    |   |   |               |-- _tables.scss
    |   |   |               |-- _theme.scss
    |   |   |               |-- _thumbnails.scss
    |   |   |               |-- _tooltip.scss
    |   |   |               |-- _type.scss
    |   |   |               |-- _utilities.scss
    |   |   |               |-- _variables.scss
    |   |   |               |-- _wells.scss
    |   |   |               |-- mixins
    |   |   |                   |-- _alerts.scss
    |   |   |                   |-- _background-variant.scss
    |   |   |                   |-- _border-radius.scss
    |   |   |                   |-- _buttons.scss
    |   |   |                   |-- _center-block.scss
    |   |   |                   |-- _clearfix.scss
    |   |   |                   |-- _forms.scss
    |   |   |                   |-- _gradients.scss
    |   |   |                   |-- _grid-framework.scss
    |   |   |                   |-- _grid.scss
    |   |   |                   |-- _hide-text.scss
    |   |   |                   |-- _image.scss
    |   |   |                   |-- _labels.scss
    |   |   |                   |-- _list-group.scss
    |   |   |                   |-- _nav-divider.scss
    |   |   |                   |-- _nav-vertical-align.scss
    |   |   |                   |-- _opacity.scss
    |   |   |                   |-- _pagination.scss
    |   |   |                   |-- _panels.scss
    |   |   |                   |-- _progress-bar.scss
    |   |   |                   |-- _reset-filter.scss
    |   |   |                   |-- _reset-text.scss
    |   |   |                   |-- _resize.scss
    |   |   |                   |-- _responsive-visibility.scss
    |   |   |                   |-- _size.scss
    |   |   |                   |-- _tab-focus.scss
    |   |   |                   |-- _table-row.scss
    |   |   |                   |-- _text-emphasis.scss
    |   |   |                   |-- _text-overflow.scss
    |   |   |                   |-- _vendor-prefixes.scss
    |   |   |-- jquery
    |   |   |   |-- .bower.json
    |   |   |   |-- AUTHORS.txt
    |   |   |   |-- bower.json
    |   |   |   |-- LICENSE.txt
    |   |   |   |-- README.md
    |   |   |   |-- dist
    |   |   |   |   |-- core.js
    |   |   |   |   |-- jquery.js
    |   |   |   |   |-- jquery.min.js
    |   |   |   |   |-- jquery.min.map
    |   |   |   |   |-- jquery.slim.js
    |   |   |   |   |-- jquery.slim.min.js
    |   |   |   |   |-- jquery.slim.min.map
    |   |   |   |-- external
    |   |   |   |   |-- sizzle
    |   |   |   |       |-- LICENSE.txt
    |   |   |   |       |-- dist
    |   |   |   |           |-- sizzle.js
    |   |   |   |           |-- sizzle.min.js
    |   |   |   |           |-- sizzle.min.map
    |   |   |   |-- src
    |   |   |       |-- .eslintrc.json
    |   |   |       |-- ajax.js
    |   |   |       |-- attributes.js
    |   |   |       |-- callbacks.js
    |   |   |       |-- core.js
    |   |   |       |-- css.js
    |   |   |       |-- data.js
    |   |   |       |-- deferred.js
    |   |   |       |-- deprecated.js
    |   |   |       |-- dimensions.js
    |   |   |       |-- effects.js
    |   |   |       |-- event.js
    |   |   |       |-- jquery.js
    |   |   |       |-- manipulation.js
    |   |   |       |-- offset.js
    |   |   |       |-- queue.js
    |   |   |       |-- selector-native.js
    |   |   |       |-- selector-sizzle.js
    |   |   |       |-- selector.js
    |   |   |       |-- serialize.js
    |   |   |       |-- traversing.js
    |   |   |       |-- wrap.js
    |   |   |       |-- ajax
    |   |   |       |   |-- jsonp.js
    |   |   |       |   |-- load.js
    |   |   |       |   |-- parseXML.js
    |   |   |       |   |-- script.js
    |   |   |       |   |-- xhr.js
    |   |   |       |   |-- var
    |   |   |       |       |-- location.js
    |   |   |       |       |-- nonce.js
    |   |   |       |       |-- rquery.js
    |   |   |       |-- attributes
    |   |   |       |   |-- attr.js
    |   |   |       |   |-- classes.js
    |   |   |       |   |-- prop.js
    |   |   |       |   |-- support.js
    |   |   |       |   |-- val.js
    |   |   |       |-- core
    |   |   |       |   |-- access.js
    |   |   |       |   |-- DOMEval.js
    |   |   |       |   |-- init.js
    |   |   |       |   |-- parseHTML.js
    |   |   |       |   |-- ready-no-deferred.js
    |   |   |       |   |-- ready.js
    |   |   |       |   |-- readyException.js
    |   |   |       |   |-- stripAndCollapse.js
    |   |   |       |   |-- support.js
    |   |   |       |   |-- var
    |   |   |       |       |-- rsingleTag.js
    |   |   |       |-- css
    |   |   |       |   |-- addGetHookIf.js
    |   |   |       |   |-- adjustCSS.js
    |   |   |       |   |-- curCSS.js
    |   |   |       |   |-- hiddenVisibleSelectors.js
    |   |   |       |   |-- showHide.js
    |   |   |       |   |-- support.js
    |   |   |       |   |-- var
    |   |   |       |       |-- cssExpand.js
    |   |   |       |       |-- getStyles.js
    |   |   |       |       |-- isHiddenWithinTree.js
    |   |   |       |       |-- rmargin.js
    |   |   |       |       |-- rnumnonpx.js
    |   |   |       |       |-- swap.js
    |   |   |       |-- data
    |   |   |       |   |-- Data.js
    |   |   |       |   |-- var
    |   |   |       |       |-- acceptData.js
    |   |   |       |       |-- dataPriv.js
    |   |   |       |       |-- dataUser.js
    |   |   |       |-- deferred
    |   |   |       |   |-- exceptionHook.js
    |   |   |       |-- effects
    |   |   |       |   |-- animatedSelector.js
    |   |   |       |   |-- Tween.js
    |   |   |       |-- event
    |   |   |       |   |-- ajax.js
    |   |   |       |   |-- alias.js
    |   |   |       |   |-- focusin.js
    |   |   |       |   |-- support.js
    |   |   |       |   |-- trigger.js
    |   |   |       |-- exports
    |   |   |       |   |-- amd.js
    |   |   |       |   |-- global.js
    |   |   |       |-- manipulation
    |   |   |       |   |-- buildFragment.js
    |   |   |       |   |-- getAll.js
    |   |   |       |   |-- setGlobalEval.js
    |   |   |       |   |-- support.js
    |   |   |       |   |-- wrapMap.js
    |   |   |       |   |-- _evalUrl.js
    |   |   |       |   |-- var
    |   |   |       |       |-- rcheckableType.js
    |   |   |       |       |-- rscriptType.js
    |   |   |       |       |-- rtagName.js
    |   |   |       |-- queue
    |   |   |       |   |-- delay.js
    |   |   |       |-- traversing
    |   |   |       |   |-- findFilter.js
    |   |   |       |   |-- var
    |   |   |       |       |-- dir.js
    |   |   |       |       |-- rneedsContext.js
    |   |   |       |       |-- siblings.js
    |   |   |       |-- var
    |   |   |           |-- arr.js
    |   |   |           |-- class2type.js
    |   |   |           |-- concat.js
    |   |   |           |-- document.js
    |   |   |           |-- documentElement.js
    |   |   |           |-- fnToString.js
    |   |   |           |-- getProto.js
    |   |   |           |-- hasOwn.js
    |   |   |           |-- indexOf.js
    |   |   |           |-- ObjectFunctionString.js
    |   |   |           |-- pnum.js
    |   |   |           |-- push.js
    |   |   |           |-- rcssNum.js
    |   |   |           |-- rnothtmlwhite.js
    |   |   |           |-- slice.js
    |   |   |           |-- support.js
    |   |   |           |-- toString.js
    |   |   |-- veams-sass
    |   |       |-- .bower.json
    |   |       |-- bower.json
    |   |       |-- LICENSE.md
    |   |       |-- helpers
    |   |       |   |-- config.js
    |   |       |   |-- _grunt
    |   |       |       |-- fileindex.js
    |   |       |       |-- version.js
    |   |       |-- resources
    |   |           |-- scss
    |   |               |-- _veams-animations.scss
    |   |               |-- _veams-helpers.scss
    |   |               |-- _veams-normalize.scss
    |   |               |-- _veams-reset.scss
    |   |               |-- _veams.scss
    |   |               |-- animations
    |   |               |   |-- feedback-effects
    |   |               |   |   |-- _fb-ani-borderMultiple.scss
    |   |               |   |   |-- _fb-ani-borderSimple.scss
    |   |               |   |   |-- _fb-ani-circleBig.scss
    |   |               |   |   |-- _fb-ani-circleDelay.scss
    |   |               |   |   |-- _fb-ani-circleRevert.scss
    |   |               |   |   |-- _fb-ani-circleShrink.scss
    |   |               |   |   |-- _fb-ani-circleSimple.scss
    |   |               |   |   |-- _fb-ani-setup.scss
    |   |               |   |-- in-out-effects
    |   |               |       |-- _io-ani--carousel.scss
    |   |               |       |-- _io-ani--cube.scss
    |   |               |       |-- _io-ani--fall.scss
    |   |               |       |-- _io-ani--flip.scss
    |   |               |       |-- _io-ani--fold-and-unfold.scss
    |   |               |       |-- _io-ani--move.scss
    |   |               |       |-- _io-ani--newspaper.scss
    |   |               |       |-- _io-ani--push-and-pull.scss
    |   |               |       |-- _io-ani--room-walls.scss
    |   |               |       |-- _io-ani--rotate-and-scale.scss
    |   |               |       |-- _io-ani--scale.scss
    |   |               |       |-- _io-ani--sides.scss
    |   |               |       |-- _io-ani--slides.scss
    |   |               |-- browser-resets
    |   |               |   |-- _buttons.scss
    |   |               |   |-- _normalize.scss
    |   |               |   |-- _reset.scss
    |   |               |-- effects
    |   |               |   |-- _arrow.scss
    |   |               |-- global
    |   |               |   |-- _font-base.scss
    |   |               |   |-- _settings.scss
    |   |               |-- layout
    |   |               |   |-- _centering.scss
    |   |               |   |-- _clearfix.scss
    |   |               |   |-- _float.scss
    |   |               |   |-- _hide.scss
    |   |               |   |-- _ib.scss
    |   |               |   |-- _omega-reset.scss
    |   |               |   |-- _pseudo.scss
    |   |               |-- _helpers
    |   |                   |-- _breakpoint.scss
    |   |                   |-- _cp.scss
    |   |                   |-- _grunticon.scss
    |   |                   |-- _keyframes.scss
    |   |                   |-- _prefix.scss
    |   |                   |-- _rem.scss
    |   |-- js
    |   |   |-- app.js
    |   |   |-- common.js
    |   |   |-- jquery-ui-1.9.2.min.js
    |   |   |-- jquery.bxslider.min.js
    |   |   |-- jquery.min.js
    |   |   |-- main.js
    |   |-- scss
    |   |   |-- .gitignore
    |   |   |-- styles.scss
    |   |   |-- styles.tmp.scss
    |   |   |-- universal.scss
    |   |   |-- global
    |   |       |-- _base.scss
    |   |       |-- _print.scss
    |   |       |-- _vars.scss
    |   |-- templating
    |       |-- data
    |       |   |-- config.json
    |       |-- layouts
    |       |   |-- lyt-default.hbs
    |       |   |-- README.md
    |       |-- pages
    |       |   |-- page1.hbs
    |       |   |-- page2.hbs
    |       |   |-- page3.hbs
    |       |   |-- page4.hbs
    |       |-- partials
    |           |-- blocks
    |           |   |-- b-sitemap.hbs
    |           |   |-- b-version.hbs
    |           |   |-- README.md
    |           |-- components
    |           |   |-- README.md
    |           |-- utilities
    |           |   |-- README.md
    |           |-- _global
    |               |-- _metadata.hbs
    |               |-- _scripts.hbs
    |               |-- _styles.hbs
    |-- server
    |   |-- main.js
    |-- test
    |   |-- helpers
    |   |   |-- html
    |   |       |-- demo.test.js
    |   |       |-- test.html
    |   |-- spec
    |       |-- e2e
    |           |-- demo.spec.js
    |-- _dist
    |   |-- page1.html
    |   |-- page2.html
    |   |-- page3.html
    |   |-- page4.html
    |   |-- css
    |   |   |-- custom.css
    |   |   |-- font-awesome.min.css
    |   |   |-- jquery-ui-1.9.2.min.css
    |   |   |-- jquery.bxslider.css
    |   |   |-- main.css
    |   |   |-- reaction.css
    |   |   |-- styles.css
    |   |   |-- styles.css.map
    |   |   |-- universal.css
    |   |   |-- universal.css.map
    |   |-- fonts
    |   |   |-- fontawesome-webfont.eot
    |   |   |-- fontawesome-webfont.svg
    |   |   |-- fontawesome-webfont.ttf
    |   |   |-- fontawesome-webfont.woff
    |   |   |-- fontawesome-webfont.woff2
    |   |   |-- FontAwesome.otf
    |   |-- img
    |   |   |-- cake-images.jpg
    |   |   |-- default_avatar.gif
    |   |   |-- facebook.png
    |   |   |-- favicon.jpg
    |   |   |-- logo.png
    |   |   |-- reaction-small.png
    |   |   |-- reactionsgif.gif
    |   |   |-- reactions_angry.png
    |   |   |-- reactions_haha.png
    |   |   |-- reactions_like.png
    |   |   |-- reactions_love.png
    |   |   |-- reactions_sad.png
    |   |   |-- reactions_wow.png
    |   |   |-- tagline.png
    |   |-- js
    |       |-- common.js
    |       |-- custom.js
    |       |-- jquery-ui-1.9.2.min.js
    |       |-- jquery.bxslider.min.js
    |       |-- jquery.min.js
    |       |-- main.js
    |       |-- reaction.js
    |       |-- vendor
    |           |-- libs.js
    |-- _output
        |-- page1.html
        |-- page2.html
        |-- page3.html
        |-- page4.html
        |-- css
        |   |-- custom.css
        |   |-- font-awesome.min.css
        |   |-- jquery-ui-1.9.2.min.css
        |   |-- jquery.bxslider.css
        |   |-- main.css
        |   |-- reaction.css
        |   |-- styles.css
        |   |-- styles.css.map
        |   |-- universal.css
        |   |-- universal.css.map
        |-- fonts
        |   |-- fontawesome-webfont.eot
        |   |-- fontawesome-webfont.svg
        |   |-- fontawesome-webfont.ttf
        |   |-- fontawesome-webfont.woff
        |   |-- fontawesome-webfont.woff2
        |   |-- FontAwesome.otf
        |-- img
        |   |-- cake-images.jpg
        |   |-- default_avatar.gif
        |   |-- facebook.png
        |   |-- favicon.jpg
        |   |-- logo.png
        |   |-- reaction-small.png
        |   |-- reactionsgif.gif
        |   |-- reactions_angry.png
        |   |-- reactions_haha.png
        |   |-- reactions_like.png
        |   |-- reactions_love.png
        |   |-- reactions_sad.png
        |   |-- reactions_wow.png
        |   |-- tagline.png
        |-- js
            |-- chart.js
            |-- common.js
            |-- custom.js
            |-- jquery-ui-1.9.2.min.js
            |-- jquery.bxslider.min.js
            |-- jquery.min.js
            |-- main.js
            |-- reaction.js
            |-- vendor
                |-- libs.js
